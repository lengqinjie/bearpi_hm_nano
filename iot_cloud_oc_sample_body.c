/*
 * Copyright (c) 2020 Nanjing Xiaoxiongpai Intelligent Technology Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "ohos_init.h"
#include "cmsis_os2.h"

#include "wifi_connect.h"
#include <queue.h>
#include <oc_mqtt_al.h>
#include <oc_mqtt_profile.h>
#include "E53_IS1.h"
#include <dtls_al.h>
#include <mqtt_al.h>


#define CONFIG_WIFI_SSID            "ruantong"                            //修改为自己的WiFi 热点账号

#define CONFIG_WIFI_PWD             "ruantong"                        //修改为自己的WiFi 热点密码

#define CONFIG_APP_SERVERIP         "121.36.42.100"

#define CONFIG_APP_SERVERPORT       "1883"

#define CONFIG_APP_DEVICEID         "60fadd36aa3bcc02873d3ac0_infrared-device-owned-by-lengqinjie"           //替换为注册设备后生成的deviceid

#define CONFIG_APP_DEVICEPWD        "40579155e9aa6650e8a10e746fa0b58a"                                     //替换为注册设备后生成的密钥

#define CONFIG_APP_LIFETIME         60     ///< seconds

#define CONFIG_QUEUE_TIMEOUT        (5*1000)

#define MSGQUEUE_OBJECTS 16 // number of Message Queue Objects

typedef enum
{
    en_msg_cmd = 0,
    en_msg_report,
}en_msg_type_t;

typedef struct
{
    char *request_id;
    char *payload;
} cmd_t;


typedef struct
{
    queue_t                     *app_msg;
    int                          connected;
}app_cb_t;
static app_cb_t  g_app_cb;

int Infrared_Status = 0;


static void deal_report_msg(void)
{
    oc_mqtt_profile_service_t service;
    oc_mqtt_profile_kv_t status;

    if(g_app_cb.connected != 1){
        return;
    }
    service.event_time = NULL;
    service.service_id = "Infrared";
    service.service_property = &status;
    service.nxt = NULL;
    
    status.key = "Infrared_Status";
    status.value = Infrared_Status ? "Intrude" : "Safe";
    status.type = EN_OC_MQTT_PROFILE_VALUE_STRING;
    status.nxt = NULL;

    oc_mqtt_profile_propertyreport(NULL,&service);
    return;
}

static void deal_get_properties(char *request_id)
{
    oc_mqtt_profile_service_t service;
    oc_mqtt_profile_kv_t status;

    if(g_app_cb.connected != 1){
        return;
    }
    service.event_time = NULL;
    service.service_id = "Infrared";
    service.service_property = &status;
    service.nxt = NULL;
    
    status.key = "Infrared_Status";
    status.value = Infrared_Status ? "Intrude" : "Safe";
    status.type = EN_OC_MQTT_PROFILE_VALUE_STRING;
    status.nxt = NULL;

    ///< do the response    
    oc_mqtt_profile_propertygetresp_t resp;     
	resp.request_id = request_id;
	resp.services = &service;
    int ret = oc_mqtt_profile_propertygetresp(NULL, &resp);
    return;
}


static int msg_rcv_callback(oc_mqtt_profile_msgrcv_t *msg)
{
    char *request_id;
	int id_len;

    if((NULL == msg)|| (msg->request_id == NULL) || 
		msg->type != EN_OC_MQTT_PROFILE_MSG_TYPE_DOWN_PROPERTYGET){
        return -1;  //某个错误码
    }

	id_len = strlen(msg->request_id);
    request_id = (char*)malloc(id_len+1);
	strcpy(request_id, msg->request_id);

	queue_push(g_app_cb.app_msg, request_id, 10);

    return 0;
}

#define FLAGS_MSK1 0x00000001U

osEventFlagsId_t evt_id;

static void Beep_Alarm(char *arg)
{
    (void)arg;
    osEventFlagsSet(evt_id, FLAGS_MSK1);
}

static int task_main_entry(void)
{
    uint32_t ret ;
    WifiConnect(CONFIG_WIFI_SSID, CONFIG_WIFI_PWD);
    dtls_al_init();
    mqtt_al_init();
    oc_mqtt_init();
    
    g_app_cb.app_msg = queue_create("queue_rcvmsg",10,1);
    if(NULL ==  g_app_cb.app_msg){
        printf("Create receive msg queue failed");
        
    }
    oc_mqtt_profile_connect_t  connect_para;
    (void) memset( &connect_para, 0, sizeof(connect_para));

    connect_para.boostrap =      0;
    connect_para.device_id =     CONFIG_APP_DEVICEID;
    connect_para.device_passwd = CONFIG_APP_DEVICEPWD;
    connect_para.server_addr =   CONFIG_APP_SERVERIP;
    connect_para.server_port =   CONFIG_APP_SERVERPORT;
    connect_para.life_time =     CONFIG_APP_LIFETIME;
    connect_para.rcvfunc =       msg_rcv_callback;
    connect_para.security.type = EN_DTLS_AL_SECURITY_TYPE_NONE;
    ret = oc_mqtt_profile_connect(&connect_para);
    if((ret == (int)en_oc_mqtt_err_ok)){
        g_app_cb.connected = 1;
        printf("oc_mqtt_profile_connect succed!\r\n");
    }
    else
    {
        printf("oc_mqtt_profile_connect faild!\r\n");
    }
    
    while (1)
    {         
    	char *request_id;
    	(void)queue_pop(g_app_cb.app_msg,(void **)&request_id,0xFFFFFFFF);
        if (NULL != request_id)
        {
			deal_get_properties(request_id);                
        } else {
			osDelay(100);
        }
    }
    return 0;
}

static int task_beep_entry(void)
{
	E53_IS1_Init();
    E53_IS1_Read_Data(Beep_Alarm);
	deal_report_msg();
	while(1)
	{
		osEventFlagsWait(evt_id, FLAGS_MSK1, osFlagsWaitAny, osWaitForever);
	    Beep_StatusSet(ON);
	    Infrared_Status = 1;
	    deal_report_msg();
	    
	    osDelay(100);

	    Beep_StatusSet(OFF);
		//deal_report_msg();
	}
	return 0;
}



static void OC_Demo(void)
{
    evt_id = osEventFlagsNew(NULL);
    if (evt_id == NULL)
    {
        printf("Falied to create EventFlags!\n");
    }
    osThreadAttr_t attr;

    attr.name = "task_main_entry";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 10240;
    attr.priority = 24;

    if (osThreadNew((osThreadFunc_t)task_main_entry, NULL, &attr) == NULL)
    {
        printf("Falied to create task_main_entry!\n");
    }

	attr.name = "task_beep_entry";  //蜂鸣器动作线程
	if (osThreadNew((osThreadFunc_t)task_beep_entry, NULL, &attr) == NULL)
    {
        printf("Falied to create task_main_entry!\n");
    }
}

APP_FEATURE_INIT(OC_Demo);

# bearpi_hm_nano

#### 介绍
鸿蒙小熊派开发板支持被安卓远程操作的补丁

#### 软件架构
1. iot_cloud_oc_sample_xxx.c 分别对应小熊派官方仓库中[对应案例的代码](https://gitee.com/bearpi/bearpi-hm_nano/tree/master/applications/BearPi/BearPi-HM_Nano/sample)
2. 如iot_cloud_oc_sample_smoke.c对应的是D8案例
3. 使用的时候将iot_cloud_oc_sample_smoke.c内容覆盖到D8案例中的iot_cloud_oc_sample.c文件
4. 然后再修改wifi用户名密码，华为云IOT平台注册的device id和device passwd
5. 然后就可以连接到华为云物联网平台，并且可以使用定制的[android APP](https://gitee.com/newzoubo/hwlot/blob/master/app/release/app-release.apk)来控制这个D8案例


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
